Name:		poco
Version:	1.10.1
Release:	1
Summary:	This is ROS melodic ros_comm's 3rdparty poco
License:	GPL
URL:		https://github.com/pocoproject/poco/releases/tag/poco-1.10.1-release
Source0:	https://github.com/pocoproject/poco/archive/poco-1.10.1-release.tar.gz
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	boost-devel
BuildRequires:	openssl-devel

%description
This is ROS melodic ros_comm's 3rdparty poco.

%prep
%setup

%install
mkdir cmake-build/
cd cmake-build/
cmake ..
make -j9 
make install
cd ..

#install
mkdir -p %{buildroot}/usr/local/
cp -r install/* %{buildroot}/usr/local/

%files
%defattr(-,root,root)
/usr/local/*

%changelog
* Thu December 31 2020 openEuler Buildteam <hanhaomin008@126.com>
- Package init
